/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Raul
 */
public class CSV {

    private String  csvFichero;
    private String  csvSeparador;
    private Integer csvCabecera;
    private String  separadorDatos;

    public CSV(String csvFichero, String csvSeparador, Integer csvCabecera, String separadorDatos) {
        this.csvFichero = csvFichero;
        this.csvSeparador = csvSeparador;
        this.csvCabecera = csvCabecera;
        this.separadorDatos = separadorDatos;
    }
    
    public boolean cargar(ArrayList<Tabla> alTablas) throws FileNotFoundException, IOException{
        
        String sLinea = "";
        String sTipoODI = "null",sTamFisico = "null";
        String sTablaPrevia = "",sTablaActual = "";
        Integer iTamFisico = 0;
        int contador = csvCabecera,contTablas = 0;
        Boolean flagError = false,flagPrimeraVez = true;
        FileReader fichero = new FileReader(csvFichero);
        BufferedReader br = new BufferedReader(fichero);
        
        ArrayList<Campos> alCampos = new ArrayList<>();

        System.out.print("\n");
        while((sLinea = br.readLine()) != null){
            
            String[] campos = sLinea.split(csvSeparador);            
            if(campos.length==0) break;
            if(campos[0].equals("Tabla")) continue;
            

            for(int i=0;i<2;i++){ 
                if(campos[i].isEmpty()||campos.length<6){
                    System.out.println("Faltan datos en la fila: "+ (contador+1));
                    flagError = true;
                    break;
                }
            }

            //Parseo de tipos de BBDD a ODI
            switch(campos[4].toUpperCase()){
                case "VARCHAR2":
                    sTipoODI="STRING";
                    //Doblamos el tamaño de los Strings para solucionar el problema de los guiones
                    iTamFisico = Integer.parseInt(campos[5].trim());
                    iTamFisico*=2;
                    sTamFisico = iTamFisico.toString();
                    if(iTamFisico>4000) sTamFisico = "4000";
                    break;
                case "NUMERIC":
                    sTipoODI="NUMERIC";
                    break;
                default:
                    System.out.println("Campo \""+campos[4]+"\" no soportado en la fila: "+ (contador+1));
                    flagError = true;
                    break;
            }

            if(!flagError){
                
                sTablaActual = campos[0];
                
                if(flagPrimeraVez){
                    sTablaPrevia = campos[0];
                    Tabla tTabla = new Tabla(sTablaActual);
                    alTablas.add(tTabla);  
                    flagPrimeraVez = false;
                }else if(!sTablaPrevia.equals(sTablaActual)){
                    Campos[] arCampos = new Campos[alCampos.size()];
                    int i=0;
                    for (Campos campo : alCampos) {
                        arCampos[i] = campo;
                        i++;
                    }
                    alCampos.clear();
                    alTablas.get(contTablas).setCampos(arCampos);
                    contTablas++;
                    sTablaPrevia = campos[0];
                    Tabla tTabla = new Tabla(sTablaActual);
                    alTablas.add(tTabla);  
                }
                
                if(sTipoODI.compareTo("STRING")==0){
                    Campos cCampo = new Campos(campos[1].trim(), sTipoODI.trim(), sTamFisico, sTamFisico, "null", "null");
                    alCampos.add(cCampo);
                }
                else if(campos[5].contains(",")){
                    String [] tipo= campos[5].split(",");                                           
                    Campos cCampo = new Campos(campos[1].trim(), sTipoODI.trim(), tipo[0].trim(), tipo[0].trim(), tipo[1].trim(), "null");  
                    alCampos.add(cCampo);
                }else{
                    Campos cCampo = new Campos(campos[1].trim(), sTipoODI.trim(), campos[5].trim(), campos[5].trim(), "null", "null");
                    alCampos.add(cCampo);
                }                                
            }
            contador++;
        }
        
        if(!flagError){
            Campos[] arCampos = new Campos[alCampos.size()];
            int i=0;
            for (Campos campo : alCampos) {
                arCampos[i] = campo;
                i++;
            }
            alCampos.clear();
            alTablas.get(contTablas).setCampos(arCampos);
            
            System.out.println("XMLs - Columas");
            for (Tabla alTabla : alTablas) {
                System.out.println(alTabla.getNombreTabla()+" - "+alTabla.getCampos().length);
            }
            System.out.println("\n");
        }

        return flagError;
    }

    public String getCsvFichero() {
        return csvFichero;
    }

    public void setCsvFichero(String csvFichero) {
        this.csvFichero = csvFichero;
    }

    public String getCsvSeparador() {
        return csvSeparador;
    }

    public void setCsvSeparador(String csvSeparador) {
        this.csvSeparador = csvSeparador;
    }

    public Integer getCsvCabecera() {
        return csvCabecera;
    }

    public void setCsvCabecera(Integer csvCabecera) {
        this.csvCabecera = csvCabecera;
    }
}

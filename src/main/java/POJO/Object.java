/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class Object
{
    @JacksonXmlProperty(localName = "class")
    private String classs;
    
    @JacksonXmlProperty(localName = "Field")
    @JacksonXmlElementWrapper(useWrapping = false)
    private Fields[] Field;

    @JacksonXmlProperty(localName = "Field")
    @JacksonXmlElementWrapper(useWrapping = false)
    public Fields[] getField ()
    {
        return Field;
    }

    public void setField (Fields[] Field)
    {
        this.Field = Field;
    }

    @JacksonXmlProperty(isAttribute=true)
    public String getClasss ()
    {
        return classs;
    }

    public void setClasss (String classs)
    {
        this.classs = classs;
    }

}
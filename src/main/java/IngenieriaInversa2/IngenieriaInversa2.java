package IngenieriaInversa2;

import POJO.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import CSV.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;

/**
 *
 * @author Raul
 */
public class IngenieriaInversa2 {

    public static String  csvFichero = "Tabla.csv";
    public static final String  csvSeparador = ";";
    public static final Integer csvCabecera = 1;
    public static String  separadorDecimal;
    
    
    public static Boolean esObjetoColumna(XML xmlEntrada,int iPosicion){
        if(xmlEntrada.getObject()[iPosicion].getClasss().equals("com.sunopsis.dwg.dbobj.SnpCol")) return true;
        else return false;
    }
    
    public static int obtenerNumeroObjetoConfiguracion(XML xmlEntrada,String Nombre){
        for(int i=1;;i++) 
            if(xmlEntrada.getObject()[i].getClasss().equals(Nombre)) return i;
    }
    
    public static void arregloFinalArchivo(String ruta) throws IOException{
        File fichero = new File(ruta);  
        Path path = Paths.get(ruta);
        Charset charset = StandardCharsets.UTF_8;
        
        //Añadir declaracion de XML
        List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
        lines.add(0, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
        Files.write(path, lines, StandardCharsets.UTF_8);
        
        //Arreglos finales cambiar CDATA        
        String content = new String(Files.readAllBytes(path), charset);
        content = content.replaceAll("String\"><!\\[CDATA\\[null\\]\\]>", "String\">null");
        Files.write(path, content.getBytes(charset));
        
        //XML Beautifier
        String contentFinal = new String(Files.readAllBytes(path), charset);
        XmlFormatter xmlF = new XmlFormatter();
        contentFinal = xmlF.format(contentFinal);
        fichero.delete();
        BufferedWriter bw = new BufferedWriter(new FileWriter(ruta));
        bw.write(contentFinal);
        bw.close();
    }
    
    public static void existeFichero(String ruta){
        File file = new File(ruta);
        if (!file.exists()){
            System.out.println("\nNo se encuentra el fichero: " + ruta + "\n");
            System.exit(0);
        }
    }
    
    public static ArrayList<TablaProcesada> crearObjsXML(XML xmlCitrix) throws IOException, IllegalArgumentException, IllegalAccessException{            
        Boolean flagError = true;        
        String sMatricula = "";
        String IntVersion = "1";
        String sGlobalId = "be131b89-91d7-4bcb-a750-44331c836921";
        Integer iITable = 1;
        Integer iPos = 1;
        int j=0,k=0,l=0,iElementosABuscar=0;  
        
        CSV csv = new CSV(csvFichero,csvSeparador,csvCabecera,separadorDecimal);
        ArrayList<Tabla> alTablas = new ArrayList<>();
        ArrayList<ObjField> alObjFields = new ArrayList<>();
        ArrayList<POJO.Object> alObjects = new ArrayList<>();
        ArrayList<TablaProcesada> alRes = new ArrayList<>();
        POJO.Object[] arObjects;
        
        flagError=csv.cargar(alTablas);
        
        if(!flagError){
            while(iElementosABuscar < 2){
                if(xmlCitrix.getObject()[0].getField()[j].getName().equals("FirstUser")){ 
                    sMatricula = xmlCitrix.getObject()[0].getField()[j].getContent();
                    iElementosABuscar++;
                }
                if(xmlCitrix.getObject()[0].getField()[j].getName().equals("ITable")){ 
                    iITable = Integer.parseInt(xmlCitrix.getObject()[0].getField()[j].getContent());
                    iElementosABuscar++;
                }
                
                j++;
            }   
            
            for(Tabla t : alTablas){
                
                Fields[] arFields = new Fields[39];
                alObjFields.clear();
                alObjects.clear();
                iPos = 0;
                
                for(Campos c : t.getCampos()){                
                    if(c.getEscala().equals("null")) c.setEscala("0");
                    ObjField objColumna = new ObjField(Integer.parseInt(c.getTamFisico()),c.getNombre(),c.getNombre(),sMatricula,sGlobalId,Integer.parseInt(IntVersion),iPos,iITable,sMatricula,Integer.parseInt(c.getTamFisico()),iPos,Integer.parseInt(c.getEscala()),c.getTipo());
                    alObjFields.add(objColumna);
                    iPos++;
                }
            
                for (ObjField col : alObjFields) {
                    k=0;

                    for(Field f : col.getClass().getFields()) {
                        Fields fFila = new Fields();
                        fFila.setName(f.getName());        
                        Type tTipo = f.getGenericType();
                        String sTipo = tTipo.getTypeName();
                        if(sTipo.equals("java.lang.Integer")) fFila.setType("com.sunopsis.sql.DbInt");
                        if(sTipo.equals("java.lang.String")) fFila.setType("java.lang.String");
                        if(f.getName().equals("FirstDate")||f.getName().equals("LastDate")) fFila.setType("java.sql.Timestamp");
                        fFila.setContent(String.valueOf(f.get(col)));                
                        arFields[k]=fFila;                               
                        k++;
                    }

                    POJO.Object objeto = new POJO.Object();
                    objeto.setClasss("com.sunopsis.dwg.dbobj.SnpCol");
                    objeto.setField(arFields.clone());

                    alObjects.add(objeto);
                }

                l=0;
                arObjects = new POJO.Object[alObjects.size()];
                for (POJO.Object object : alObjects) {
                    arObjects[l]=object;
                    l++;
                }
                
                TablaProcesada tTabla = new TablaProcesada(t.getNombreTabla(),arObjects);
                alRes.add(tTabla);            
            }            
        }else{
            System.out.println("\nPor favor arregle los errores indicados en el CSV");
            alRes.clear();
        }
        
        return alRes;
    }
    
    public static void crearXMLs(String sCitrix) throws IOException, IllegalArgumentException, IllegalAccessException{
        ObjectMapper xmlMapper = new XmlMapper();
        
        XML xmlCitrix = xmlMapper.readValue(new File(sCitrix), XML.class);   
        ArrayList<TablaProcesada> alTablasProcesadas = crearObjsXML(xmlCitrix);
        
        for (TablaProcesada tabla : alTablasProcesadas) {
            
            xmlCitrix = xmlMapper.readValue(new File(sCitrix), XML.class); 
            POJO.Object[] arObjectsConfig = xmlCitrix.getObject();
            ArrayList<POJO.Object> alObjsConfig = new ArrayList<>();
            ArrayList<POJO.Object> alObjs = new ArrayList<>();            
            
            //Cambios de tamaño y nombre de la tabla..
            int iNumObjCitrix = obtenerNumeroObjetoConfiguracion(xmlCitrix, "com.sunopsis.dwg.DwgExportSummary");
            int iElementosABuscar=0;
            int i=0;
            while(iElementosABuscar < 1){
                if(xmlCitrix.getObject()[iNumObjCitrix].getField()[i].getName().equals("OtherObjectsNb")){
                    xmlCitrix.getObject()[iNumObjCitrix].getField()[i].setContent(String.valueOf(tabla.getObjetos().length));
                    iElementosABuscar++;
                }
                i++;
            }
            //Cambios de tamaño y nombre de la tabla..
            iElementosABuscar=0;
            i=0;
            while(iElementosABuscar < 5){
                if(xmlCitrix.getObject()[0].getField()[i].getName().equals("ResName")){
                    xmlCitrix.getObject()[0].getField()[i].setContent(tabla.getsNombreTabla()+"_#GLOBAL.V_PARTITION_VALUE.txt");
                    iElementosABuscar++;
                }
                if(xmlCitrix.getObject()[0].getField()[i].getName().equals("TableAlias")){
                    String sAlias = "";
                    sAlias+=String.valueOf(tabla.getsNombreTabla().toUpperCase().charAt(0));
                    sAlias+=String.valueOf(tabla.getsNombreTabla().toUpperCase().charAt(1));
                    sAlias+=String.valueOf(tabla.getsNombreTabla().toUpperCase().charAt(2));
                    xmlCitrix.getObject()[0].getField()[i].setContent(sAlias);
                    iElementosABuscar++;
                }
                if(xmlCitrix.getObject()[0].getField()[i].getName().equals("TableName")){
                    xmlCitrix.getObject()[0].getField()[i].setContent(tabla.getsNombreTabla());
                    iElementosABuscar++;
                }
                if(xmlCitrix.getObject()[0].getField()[i].getName().equals("WsName")){
                    xmlCitrix.getObject()[0].getField()[i].setContent(tabla.getsNombreTabla());
                    iElementosABuscar++;
                }
                if(xmlCitrix.getObject()[0].getField()[i].getName().equals("WsEntityName")){
                    xmlCitrix.getObject()[0].getField()[i].setContent("WS"+tabla.getsNombreTabla());
                    iElementosABuscar++;
                }
                i++;
            }

            //Inserto los objetos de las columnas en el sitio correcto del XML
            alObjs.clear();
            alObjsConfig.clear();
            alObjsConfig.addAll(Arrays.asList(arObjectsConfig));
            alObjs.addAll(Arrays.asList(tabla.getObjetos()));
            POJO.Object[] arObjFinales;
            
            int j=1;
            for (POJO.Object obj : alObjs) {
                alObjsConfig.add(j, obj);
                j++;
            }

            j=0;
            arObjFinales = new POJO.Object[alObjsConfig.size()];
            for (POJO.Object object : alObjsConfig) {
                arObjFinales[j] = object;
                j++;
            }

            xmlCitrix.setObject(arObjFinales); 
            
            //Borrado previo
            String ruta = "TAB_"+tabla.getsNombreTabla()+".xml.bak";
            File fichero = new File(ruta);
            fichero.delete();

            //Salida
            xmlMapper.writeValue(new File(ruta),xmlCitrix);

            //Arreglos
            arregloFinalArchivo(ruta);
        }
    }
    
    public static void main(String[] args) throws IOException, IllegalArgumentException, IllegalAccessException {
        
        if(args.length != 2){
            System.out.println("Necesarios 2 parametros: XMLPlantillaCitrix CSVTablas");
            System.exit(0);
        }else{
            separadorDecimal = ".";
            existeFichero(args[0]);
            existeFichero(args[1]);
            csvFichero = args[1];
            crearXMLs(args[0]); 
        }
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

/**
 *
 * @author Raul
 */
public class Tabla {

    private String nombreTabla;
    private Campos[] campos;

    public Tabla(String nombreTabla, Campos[] campos) {
        this.nombreTabla = nombreTabla;
        this.campos = campos;
    }
    
    public Tabla(String nombreTabla) {
        this.nombreTabla = nombreTabla;
    }

    public String getNombreTabla() {
        return nombreTabla;
    }

    public void setNombreTabla(String nombreTabla) {
        this.nombreTabla = nombreTabla;
    }

    public Campos[] getCampos() {
        return campos;
    }

    public void setCampos(Campos[] campos) {
        this.campos = campos;
    }

    
}

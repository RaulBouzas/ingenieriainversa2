/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

/**
 *
 * @author Raul
 */
public class Campos {
    
    private String nombre;
    private String tipo;
    private String tamFisico;
    private String longitud;
    private String escala;
    private String separador;

    public Campos(String nombre, String tipo, String tamFisico, String longitud, String escala, String separador) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.tamFisico = tamFisico;
        this.longitud = longitud;
        this.escala = escala;
        this.separador = separador;
    }

    public Campos(String nombre, String tipo, String tamFisico) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.tamFisico = tamFisico;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String campo) {
        this.nombre = campo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTamFisico() {
        return tamFisico;
    }

    public void setTamFisico(String tamFisico) {
        this.tamFisico = tamFisico;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getEscala() {
        return escala;
    }

    public void setEscala(String escala) {
        this.escala = escala;
    }

    public String getSeparador() {
        return separador;
    }

    public void setSeparador(String separador) {
        this.separador = separador;
    }
    
    
}

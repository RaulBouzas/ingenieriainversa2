/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Encryption
{
    @JacksonXmlProperty(localName = "algorithm")
    private String algorithm;
    
    @JacksonXmlProperty(localName = "keyLength")
    private String keyLength;
    
    @JacksonXmlProperty(localName = "exportKeyHash")
    private String exportKeyHash;
    
    @JacksonXmlProperty(localName = "keyVect")
    private String keyVect;

    @JacksonXmlProperty(localName = "exportKeySalt")
    private String exportKeySalt;

    @JacksonXmlProperty(localName = "containsCipherText")
    private String containsCipherText;


    @JacksonXmlProperty(isAttribute=true)
    public String getKeyVect ()
    {
        return keyVect;
    }

    public void setKeyVect (String keyVect)
    {
        this.keyVect = keyVect;
    }

    @JacksonXmlProperty(isAttribute=true)
    public String getExportKeyHash ()
    {
        return exportKeyHash;
    }

    public void setExportKeyHash (String exportKeyHash)
    {
        this.exportKeyHash = exportKeyHash;
    }

    @JacksonXmlProperty(isAttribute=true)
    public String getExportKeySalt ()
    {
        return exportKeySalt;
    }

    public void setExportKeySalt (String exportKeySalt)
    {
        this.exportKeySalt = exportKeySalt;
    }

    @JacksonXmlProperty(isAttribute=true)
    public String getContainsCipherText ()
    {
        return containsCipherText;
    }

    public void setContainsCipherText (String containsCipherText)
    {
        this.containsCipherText = containsCipherText;
    }

    @JacksonXmlProperty(isAttribute=true)
    public String getAlgorithm ()
    {
        return algorithm;
    }

    public void setAlgorithm (String algorithm)
    {
        this.algorithm = algorithm;
    }

    @JacksonXmlProperty(isAttribute=true)
    public String getKeyLength ()
    {
        return keyLength;
    }

    public void setKeyLength (String keyLength)
    {
        this.keyLength = keyLength;
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Admin
{
    @JacksonXmlProperty(localName = "RepositoryVersion")
    private String RepositoryVersion;
    
    @JacksonXmlProperty(localName = "IsLegacyIdCompatible")
    private String IsLegacyIdCompatible;

    @JacksonXmlProperty(localName = "IsLegacyIdCompatible",isAttribute=true)
    public String getIsLegacyIdCompatible ()
    {
        return IsLegacyIdCompatible;
    }

    public void setIsLegacyIdCompatible (String IsLegacyIdCompatible)
    {
        this.IsLegacyIdCompatible = IsLegacyIdCompatible;
    }

    @JacksonXmlProperty(localName = "RepositoryVersion",isAttribute=true)
    public String getRepositoryVersion ()
    {
        return RepositoryVersion;
    }

    public void setRepositoryVersion (String RepositoryVersion)
    {
        this.RepositoryVersion = RepositoryVersion;
    }
}
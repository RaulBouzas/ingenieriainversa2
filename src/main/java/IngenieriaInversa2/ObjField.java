/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IngenieriaInversa2;

/**
 *
 * @author Raul
 */
public class ObjField {
    
    public Integer Bytes;
    public String CheckFlow;
    public String CheckStat;
    public String ColDecSep;
    public String ColFormat;
    public String ColHeading;
    public String ColMandatory;
    public String ColName;
    public String ColNullIfErr;
    public String DefValue;
    public String ExtVersion;
    public Integer FilePos;
    public String FirstDate;
    public String FirstUser;
    public String GlobalId;
    public String HbaseEncoding;
    public String IndChange;
    public String IndHiveCluster;
    public String IndHivePartition;
    public String IndHiveSort;
    public String IndIn;
    public String IndOut;
    public String IndWrite;
    public String IndWsInsert;
    public String IndWsSelect;
    public String IndWsUpdate;
    public Integer IntVersion;
    public Integer ICol;
    public Integer ISrcCol;
    public Integer ITable;
    public Integer ITxtColDesc;
    public String LastDate;
    public String LastUser;
    public Integer Longc;
    public Integer Pos;
    public String RecCodeList;
    public Integer Scalec;
    public String ScdColType;
    public String SourceDt;

    public ObjField(Integer Bytes, String ColHeading, String ColName, String FirstUser, String GlobalId, Integer IntVersion, Integer ICol, Integer ITable, String LastUser, Integer Longc, Integer Pos, Integer Scalec, String SourceDt) {
        this.Bytes = Bytes;
        this.CheckFlow = "0";
        this.CheckStat = "0";
        this.ColDecSep = "null";
        this.ColFormat = "null";
        this.ColHeading = ColHeading;
        this.ColMandatory = "0";
        this.ColName = ColName;
        this.ColNullIfErr = "";
        this.DefValue = "null";
        this.ExtVersion = "null";
        this.FilePos = 0; //null
        this.FirstDate = "2017-01-09 11:02:21.0";
        this.FirstUser = FirstUser;
        this.GlobalId = GlobalId;
        this.HbaseEncoding = "null";
        this.IndChange = "I";
        this.IndHiveCluster = "null";
        this.IndHivePartition = "null";
        this.IndHiveSort = "null";
        this.IndIn = "null";
        this.IndOut = "null";
        this.IndWrite = "1";
        this.IndWsInsert = "1";
        this.IndWsSelect = "1";
        this.IndWsUpdate = "1";
        this.IntVersion = IntVersion;
        this.ICol = ICol;
        this.ISrcCol = 0; //null
        this.ITable = ITable;
        this.ITxtColDesc = 0; //null
        this.LastDate = "2017-01-09 11:02:21.0";
        this.LastUser = LastUser;
        this.Longc = Longc;
        this.Pos = Pos;
        this.RecCodeList = "null";
        this.Scalec = Scalec;
        this.ScdColType = "null";
        this.SourceDt = SourceDt;
    }

    public Integer getBytes() {
        return Bytes;
    }

    public void setBytes(Integer Bytes) {
        this.Bytes = Bytes;
    }

    public String getCheckFlow() {
        return CheckFlow;
    }

    public void setCheckFlow(String CheckFlow) {
        this.CheckFlow = CheckFlow;
    }

    public String getCheckStat() {
        return CheckStat;
    }

    public void setCheckStat(String CheckStat) {
        this.CheckStat = CheckStat;
    }

    public String getColDecSep() {
        return ColDecSep;
    }

    public void setColDecSep(String ColDecSep) {
        this.ColDecSep = ColDecSep;
    }

    public String getColFormat() {
        return ColFormat;
    }

    public void setColFormat(String ColFormat) {
        this.ColFormat = ColFormat;
    }

    public String getColHeading() {
        return ColHeading;
    }

    public void setColHeading(String ColHeading) {
        this.ColHeading = ColHeading;
    }

    public String getColMandatory() {
        return ColMandatory;
    }

    public void setColMandatory(String ColMandatory) {
        this.ColMandatory = ColMandatory;
    }

    public String getColName() {
        return ColName;
    }

    public void setColName(String ColName) {
        this.ColName = ColName;
    }

    public String getColNullIfErr() {
        return ColNullIfErr;
    }

    public void setColNullIfErr(String ColNullIfErr) {
        this.ColNullIfErr = ColNullIfErr;
    }

    public String getDefValue() {
        return DefValue;
    }

    public void setDefValue(String DefValue) {
        this.DefValue = DefValue;
    }

    public String getExtVersion() {
        return ExtVersion;
    }

    public void setExtVersion(String ExtVersion) {
        this.ExtVersion = ExtVersion;
    }

    public Integer getFilePos() {
        return FilePos;
    }

    public void setFilePos(Integer FilePos) {
        this.FilePos = FilePos;
    }

    public String getFirstDate() {
        return FirstDate;
    }

    public void setFirstDate(String FirstDate) {
        this.FirstDate = FirstDate;
    }

    public String getFirstUser() {
        return FirstUser;
    }

    public void setFirstUser(String FirstUser) {
        this.FirstUser = FirstUser;
    }

    public String getGlobalId() {
        return GlobalId;
    }

    public void setGlobalId(String GlobalId) {
        this.GlobalId = GlobalId;
    }

    public String getHbaseEncoding() {
        return HbaseEncoding;
    }

    public void setHbaseEncoding(String HbaseEncoding) {
        this.HbaseEncoding = HbaseEncoding;
    }

    public String getIndChange() {
        return IndChange;
    }

    public void setIndChange(String IndChange) {
        this.IndChange = IndChange;
    }

    public String getIndHiveCluster() {
        return IndHiveCluster;
    }

    public void setIndHiveCluster(String IndHiveCluster) {
        this.IndHiveCluster = IndHiveCluster;
    }

    public String getIndHivePartition() {
        return IndHivePartition;
    }

    public void setIndHivePartition(String IndHivePartition) {
        this.IndHivePartition = IndHivePartition;
    }

    public String getIndHiveSort() {
        return IndHiveSort;
    }

    public void setIndHiveSort(String IndHiveSort) {
        this.IndHiveSort = IndHiveSort;
    }

    public String getIndIn() {
        return IndIn;
    }

    public void setIndIn(String IndIn) {
        this.IndIn = IndIn;
    }

    public String getIndOut() {
        return IndOut;
    }

    public void setIndOut(String IndOut) {
        this.IndOut = IndOut;
    }

    public String getIndWrite() {
        return IndWrite;
    }

    public void setIndWrite(String IndWrite) {
        this.IndWrite = IndWrite;
    }

    public String getIndWsInsert() {
        return IndWsInsert;
    }

    public void setIndWsInsert(String IndWsInsert) {
        this.IndWsInsert = IndWsInsert;
    }

    public String getIndWsSelect() {
        return IndWsSelect;
    }

    public void setIndWsSelect(String IndWsSelect) {
        this.IndWsSelect = IndWsSelect;
    }

    public String getIndWsUpdate() {
        return IndWsUpdate;
    }

    public void setIndWsUpdate(String IndWsUpdate) {
        this.IndWsUpdate = IndWsUpdate;
    }

    public Integer getIntVersion() {
        return IntVersion;
    }

    public void setIntVersion(Integer IntVersion) {
        this.IntVersion = IntVersion;
    }

    public Integer getICol() {
        return ICol;
    }

    public void setICol(Integer ICol) {
        this.ICol = ICol;
    }

    public Integer getISrcCol() {
        return ISrcCol;
    }

    public void setISrcCol(Integer ISrcCol) {
        this.ISrcCol = ISrcCol;
    }

    public Integer getITable() {
        return ITable;
    }

    public void setITable(Integer ITable) {
        this.ITable = ITable;
    }

    public Integer getITxtColDesc() {
        return ITxtColDesc;
    }

    public void setITxtColDesc(Integer ITxtColDesc) {
        this.ITxtColDesc = ITxtColDesc;
    }

    public String getLastDate() {
        return LastDate;
    }

    public void setLastDate(String LastDate) {
        this.LastDate = LastDate;
    }

    public String getLastUser() {
        return LastUser;
    }

    public void setLastUser(String LastUser) {
        this.LastUser = LastUser;
    }

    public Integer getLongc() {
        return Longc;
    }

    public void setLongc(Integer Longc) {
        this.Longc = Longc;
    }

    public Integer getPos() {
        return Pos;
    }

    public void setPos(Integer Pos) {
        this.Pos = Pos;
    }

    public String getRecCodeList() {
        return RecCodeList;
    }

    public void setRecCodeList(String RecCodeList) {
        this.RecCodeList = RecCodeList;
    }

    public Integer getScalec() {
        return Scalec;
    }

    public void setScalec(Integer Scalec) {
        this.Scalec = Scalec;
    }

    public String getScdColType() {
        return ScdColType;
    }

    public void setScdColType(String ScdColType) {
        this.ScdColType = ScdColType;
    }

    public String getSourceDt() {
        return SourceDt;
    }

    public void setSourceDt(String SourceDt) {
        this.SourceDt = SourceDt;
    }

    
    
}

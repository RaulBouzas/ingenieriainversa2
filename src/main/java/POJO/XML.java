/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "SunopsisExport")
public class XML
{
    @JacksonXmlProperty(localName = "Admin")
    @JacksonXmlElementWrapper(useWrapping = false)
    private Admin Admin;
    
    @JacksonXmlProperty(localName = "Encryption")
    @JacksonXmlElementWrapper(useWrapping = false)
    private Encryption Encryption;

    @JacksonXmlProperty(localName = "Object")
    @JacksonXmlElementWrapper(useWrapping = false)
    private Object[] Object;

    @JacksonXmlProperty(localName = "Admin",isAttribute=true)
    public Admin getAdmin() {
        return Admin;
    }

    public void setAdmin(Admin Admin) {
        this.Admin = Admin;
    }

    @JacksonXmlProperty(localName = "Encryption",isAttribute=true)
    public Encryption getEncryption() {
        return Encryption;
    }

    public void setEncryption(Encryption Encryption) {
        this.Encryption = Encryption;
    }

    @JacksonXmlProperty(localName = "Object",isAttribute=true)
    @JacksonXmlElementWrapper(useWrapping = false)
    public Object[] getObject() {
        return Object;
    }

    public void setObject(Object[] Object) {
        this.Object = Object;
    }



}
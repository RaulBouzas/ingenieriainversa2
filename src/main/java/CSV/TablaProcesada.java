/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSV;

import POJO.Object;

/**
 *
 * @author Raul
 */
public class TablaProcesada {
    
    private String sNombreTabla;
    private POJO.Object[] objetos;

    public TablaProcesada(String sNombreTabla, Object[] objetos) {
        this.sNombreTabla = sNombreTabla;
        this.objetos = objetos;
    }
    
    public String getsNombreTabla() {
        return sNombreTabla;
    }

    public void setsNombreTabla(String sNombreTabla) {
        this.sNombreTabla = sNombreTabla;
    }

    public Object[] getObjetos() {
        return objetos;
    }

    public void setObjetos(Object[] objetos) {
        this.objetos = objetos;
    }
    
}
